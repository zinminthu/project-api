<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use Validator;
class StudentController extends Controller
{
    public function index()
    {
        $student = Student::all();

        return $student->toArray();
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|min:3',
            'email' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        // dd($validator);

        // $validator = $this->validate($request, [
        //     'name' => 'required',
        //     'email' => 'required'
        // ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $student = Student::create($request->all());

        return response()->json($student, 201);
    }

    public function select($id)
    {
        $student = Student::find($id);

        if (is_null($student)) {
            return response()->json(["mesage" => "Request Not Foune"],200);
        }
        return $student->toArray();
    }

    public function update(Request $request, $id)
    {
        // $student->update($request->all());
        $student = Student::find($id);

        if(is_null($student)) {
            return response()->json(["mesage" => "Request Not Foune"], 200);
        }

        return response()->json($student, 200);
    }

    public function delete(Request $request, $id)
    {
        // $student->delete();
        $student = Student::find($id);

        if(is_null($student)) {
            return response()->json(["mesage" => "Request Not Foune"], 200);
        }

        $student->delete();

        return response()->json("Deleted Successful", 201);
    }
}
