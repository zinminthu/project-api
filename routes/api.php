<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('country', 'CountryController@index');
Route::post('country', 'CountryController@store');
Route::get('country/{id}', 'CountryController@selectId');
Route::put('country/{country}', 'CountryController@update');
Route::delete('country/{country}', 'CountryController@destroy');
// Route::delete('country/{country}', function() {
//     return "ok";
// });

//Custom Route
Route::get('student', 'StudentController@index');
Route::post('student', 'StudentController@store');
Route::get('student/{id}', 'StudentController@select');
Route::put('student/{id}', 'StudentController@update');
Route::delete('student/{id}', 'StudentController@delete');

//Group routes
Route::group(['prefix' => 'v1', 'namespace'=>'API\v1'], function() {

    Route::get('student', 'StudentController@index');
    Route::post('student', 'StudentController@store');
    Route::get('student/{id}', 'StudentController@show');
    Route::put('student/{id}', 'StudentController@update');

    //Resource Routes
    Route::apiResource('profile', 'ProfileController');

});
